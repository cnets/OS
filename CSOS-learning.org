# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018, 2021 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+LATEX_HEADER: \addbibresource{literature/education.bib}

# Create agenda slide with 1st-level headings.
#+OPTIONS: toc:1

# Do not show section numbers.
#+OPTIONS: num:nil

#+TITLE: Learning and Teaching

#+KEYWORDS: learning, teaching, active learning, deliberate practice
#+DESCRIPTION: Explanation of active learning and teaching with flipped classroom and JiTT

* Introduction
   #+ATTR_REVEAL: :frag (appear)
   1. Think of something you are really good at
      - Write it down (won’t be shared with anyone)
   2. Briefly describe how you got to be good at that thing
      - One or two words
   3. Submit how you got to be good at
      [[https://pingo.coactum.de/550323][Pingo]] (pingo.coactum.de → 550323)
      [[./img/qr-pingo-csos2021.png]]

   #+ATTR_HTML: :class slide-source
   (Source of activity: cite:SEI14)

* Learning
** Brain ≈ Muscle
   #+ATTR_REVEAL: :frag (appear)
   - Learning involves brain’s *long term memory*
     {{{reveallicense("./figures/3d-man/brain_teacher.meta")}}}
   - Long term memory needs *repeated* retrieval and practice
     - Spaced out *over time*
     - Effect: Changes in brain’s *proteins*
   - (Learning does *not* happen [solely] in lectures)

** Deliberate Practice
   Characteristics of *Deliberate Practice* to acquire expert skills
   (cite:Eri08, see also cite:EKT93,SEI14)

   1. Task with *well-defined goal*
   2. Individual *motivated* to improve
   3. *Feedback* on current performance
   4. Ample opportunities for *repetition* and *gradual refinements*

   #+ATTR_REVEAL: :frag (appear)
   [[color:gray][(Traditional lecturing is “teaching by telling”, does not share *any* characteristic of Deliberate Practice)]]
   #+begin_notes
   - First, go through enumeration
   - Then, refer back to practive vs talent
     - 10,000 hours to compete internationally in variety of domains
     - 10,000 h / 40h per week / 50 weeks = 5 years
   - Finally, “teaching by telling”
   #+end_notes

** Active Learning
   - *Active Learning* increases student performance in science,
     engineering, and mathematics (cite:FEM+14)
     - Active Learning is an umbrella term for diverse
       interventions
       - Group problem-solving
       - Worksheets or tutorials completed during class
       - Use of personal response systems with or without peer instruction
       - Studio or workshop course designs
     - Notice: Above interventions share at least 3 of the 4
       characteristics of Deliberate Practice
       - (Motivation may increase, but ultimately rests with *you*)

** Quotes from Experts
   - On cite:FEM+14
     - Carl Wieman, Nobel Prize in Physics 2001
       #+ATTR_REVEAL: :frag (appear)
       - “[[https://blogs.scientificamerican.com/budding-scientist/stop-lecturing-me-in-college-science/][A lecture is basically a talking textbook]]”
       - In cite:Wie14: “However, in undergraduate STEM education, we have the curious situation that, although more effective teaching methods have been overwhelmingly demonstrated, most STEM courses are still taught by lectures—the pedagogical equivalent of bloodletting.”
     #+ATTR_REVEAL: :frag appear
     - Eric Mazur, Harvard physicist
       - “[[https://www.sciencemag.org/news/2014/05/lectures-arent-just-boring-theyre-ineffective-too-study-finds][This is a really important article—the impression I get is that it’s almost unethical to be lecturing if you have this data]]”
   #+ATTR_REVEAL: :frag appear
   - cite:SR17: “Saying Goodbye to Lectures
     in Medical School—Paradigm Shift or Passing Fad?”
     - “60 slides in 45 minutes may seem like an efficient way to
       teach, but it is unlikely to be an effective way to learn”

* CSOS Approach
** Initial Problem and Improvement
   :PROPERTIES:
   :CUSTOM_ID: jitt-motivation
   :END:
   - 2016: Classroom response system revealed lack of student understanding
     - Yet, no in-class discussions, leaving me frustrated
       - Waste of our time
   - After introduction of JiTT: Situation improved

   {{{reveallicense("./figures/org/jitt/JiTT-Java-MX.meta","33rh",nil,none)}}}

** General Improvements
   :PROPERTIES:
   :CUSTOM_ID: jitt-improvements
   :END:
   {{{reveallicense("./figures/org/jitt/JiTT-Understanding.meta","60rh",nil,none)}}}

** Changes for 2021
   - In 2016, I taught CSOS in its entirety
     - With lots of in-class quizzes of questionable effect, as just
       explained
   - In subsequent years, Prof. Dr. Vossen taught CS part, I OS
     - With slightly different flipped classroom approaches
   - This year, I’ll teach CSOS again in its entirety
     - Again with different formats for CS and OS
       - Reuse of CS videos from 2020
       - HTML presentations such as this one for OS
       - With evaluation of both formats
     - For the first time, with uniform support with JiTT quizzes in
       both parts, explained next

* Just-In-Time Teaching (JiTT)
** Overview
   - JiTT
     - Teaching and learning strategy based on web-based study
       assignments (self-learning) and active learner classroom
       - See [[https://en.wikipedia.org/wiki/Just_in_Time_Teaching][JiTT on Wikipedia]]
       - cite:MSN16 demonstrates improved learning for statistics courses
     - Instance of *active* learning, which leads to improved
       learning in general cite:FEM+14
     - Instance of flipped/inverted classrooms cite:LPT00,BV13
       - In-class and at-home events flipped
       - Individual computer-based instruction paired with in-class (group) activities
         - Individual learning shaped by individual background and preferences
         - Lectures to discuss questions and work on exercises

** Feedback Cycles with JiTT
   :PROPERTIES:
   :CUSTOM_ID: jitt-feedback-cycles
   :END:
   {{{reveallicense("./figures/teaching/JiTT-feedback-cycles.meta","55rh",nil)}}}

* Lessons Learned
** Sample Feedback
   - Misunderstandings
     - “JiTT destroys our freedom!”
     - “JiTT tasks are too difficult/open!”
   #+ATTR_REVEAL: :frag appear
   - Encouragement
     - “JiTT is/was a very good idea and was very helpful to understand
       the course’s content”
     - “The JiTT-Assignment in combination with the lecture helped to
       understand the topics a lot!”
     - “Please continue with this type of lecuture!”

** Benefits and Challenges
   #+ATTR_REVEAL: :frag (appear)
   - Benefits
     #+ATTR_REVEAL: :frag (appear)
     - Much more *fun* in meetings with prepared students
       - Sometimes unbelievably good solutions
     - JiTT tasks helped *tremendously* to identify misunderstandings
       and improve self-study material
       - From wording to new larger units
   - Challenges
     #+ATTR_REVEAL: :frag (appear)
     - Regarding students
       - Participation in class and in JiTT assignments
       - Workload expectations
     - Regarding myself
       - Increased awareness of hurdles for students

** On Last Year’s OS Evaluation
   - Only 18 students took part
   - Suggestions for specific challenging topics to be incorporated
   - Opinions on OS presentations were split
     - “The html presentation format is so poorly.”
     - “The presentation format was good.”
     - “Overall, I really liked your class, even though it really took me some time to get used to the new teaching method.”

* Final Remarks
** On Expectations
   - cite:D+19 Actual learning vs feeling of learning
     #+ATTR_REVEAL: :frag appear
     - “[[https://www.eurekalert.org/pub_releases/2019-09/hu-lil090519.php][The effort involved in active learning can be misinterpreted as a sign of poor learning. On the other hand, a superstar lecturer can explain things in such a way as to make students feel like they are learning more than they actually are.]]”
     #+ATTR_REVEAL: :frag (appear)
     - [[https://clarissasorensenunruh.com/2019/09/13/statistics-in-education-its-harder-than-it-looks/][Questions regarding statistical rigor]]
     - One conclusion: Explain approach to students

** JiTT in CSOS 2021
   - Tuesday sessions are primarily reserved for first two tasks of
     exercise sheets.
     - We will use [[https://frag.jetzt/][frag.jetzt]] to collect
       questions anonymously.
   - Please use Learnweb for asynchronous discussions
     - What do you think of MoodleOverflow?
     - Use an anonymous pad?


#+INCLUDE: "backmatter.org"
