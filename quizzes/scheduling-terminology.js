quizSchedulingTerminology = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Scheduling and thread states",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select the correct statement about scheduling.",
            "a": [
                {"option": "If a thread’s time slice runs out, the scheduler thread takes over.", "correct": false},
                {"option": "If a thread’s time slice runs out, the OS blocks it.", "correct": false},
                {"option": "Runnable threads are eligible for scheduling decisions.", "correct": true},
                {"option": "After the OS changed a thread state from blocked to runnable, it dispatches that thread to run on a CPU core.", "correct": false},
                {"option": "After the OS preempted a thread, it changes its state to blocked.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No.</span> A “scheduler thread” does not exist; for other answers, maybe revisit the <a href=\"#slide-state-transitions\">state diagram</a> or ask for help?</p>" // no comma here
        }
    ]
};
