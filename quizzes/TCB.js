quizTCB = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Thread Control Blocks",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about TCBs",
            "a": [
                {"option": "The OS manages a TCB for each thread.", "correct": true},
                {"option": "TCBs contain state information to continue preempted threads later on.", "correct": true},
                {"option": "The TCB points to a page table.", "correct": false},
                {"option": "The TCB includes information for scheduling and about blocking events.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most or all statements are correct.)</span> What information may be specific per thread and what may be shared per process?  Maybe <a href=\"Operating-Systems-Threads.html#slide-tcb\">revisit this slide</a>?</p>" // no comma here
        }
    ]
};
